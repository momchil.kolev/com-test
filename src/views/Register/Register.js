import React, { useState } from "react"

import Fieldset from "../../components/Fieldset/Fieldset"
import Container from "../../components/Container/Container"
import styles from "./Register.module.scss"

const Register = props => {
	const [state, setState] = useState({
		name: "",
		email: "",
		address1: "",
		city: "",
		state: "",
		zip: "",
		password: "",
		"confim-password": "",
		"contact-phone": "",
		address2: "",
		name2: ""
	})

	const handleChange = inputName => e => {
		setState({
			...state,
			[inputName]: e.target.value
		})
	}

	return (
		<div className={styles.register}>
			<h1>Register route</h1>
			<Container>
				<form id="registration-form">
					<div className={styles["register-form"]}>
						<div className={styles["left-column"]}>
							<Fieldset
								form="registration-form"
								label="name"
								type="text"
								name="name"
								handleChange={handleChange("name")}
							/>
							<Fieldset
								form="registration-form"
								label="email"
								type="email"
								name="email"
								handleChange={handleChange("email")}
							/>
							<Fieldset
								form="registration-form"
								label="Address 1"
								type="text"
								name="address1"
								handleChange={handleChange("address1")}
							/>
							<div className={styles.row}>
								<Fieldset
									form="registration-form"
									label="city"
									type="text"
									name="city"
									handleChange={handleChange("city")}
								/>
								<Fieldset
									form="registration-form"
									label="state"
									type="text"
									name="state"
									select="true"
									options={["Alabama", "California", "New York", "Texas"]}
									handleChange={handleChange("state")}
								/>
								<Fieldset
									form="registration-form"
									label="zip"
									type="text"
									name="zip"
									handleChange={handleChange("zip")}
								/>
							</div>
						</div>
						<div className={styles["right-column"]}>
							<div className={styles.row}>
								<Fieldset
									form="registration-form"
									label="password"
									type="password"
									name="password"
									handleChange={handleChange("password")}
								/>
								<Fieldset
									form="registration-form"
									label="confirm password"
									type="password"
									name="confirm-password"
									handleChange={handleChange("confirm-password")}
								/>
							</div>
							<div className={styles.row}>
								<Fieldset
									form="registration-form"
									label="Contact"
									type="text"
									name="phone1"
									handleChange={handleChange("phone1")}
								/>
								<Fieldset
									form="registration-form"
									label="Phone"
									type="text"
									name="phone2"
									handleChange={handleChange("phone2")}
								/>
								<Fieldset
									form="registration-form"
									// label="Address 2"
									type="text"
									name="phone3"
									handleChange={handleChange("phone3")}
								/>
							</div>
							<Fieldset
								form="registration-form"
								label="Address 2"
								type="password"
								name="address2"
								handleChange={handleChange("address2")}
							/>
						</div>
					</div>
				</form>
			</Container>
			<button onClick={() => console.log("state is", state)}>Button</button>
		</div>
	)
}

export default Register
