import React from "react"

import styles from "./Fieldset.module.scss"

export default props => {
	const {
		form,
		handleChange,
		label,
		name,
		options,
		select,
		type,
		value
	} = props
	let field = ""

	if (select) {
		field = (
			<select>
				{options.map(option => (
					<option value={option} key={option}>
						{option}
					</option>
				))}
			</select>
		)
	} else {
		field = (
			<input type={type} name={name} value={value} onChange={handleChange} />
		)
	}

	return (
		<fieldset className={styles["form-group"]} form={form}>
			<label htmlFor={label || ""}>{label ? label.toUpperCase() : ""}</label>
			{field}
		</fieldset>
	)
}
